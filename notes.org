* goals

  + update emacs mu4e automatically when a new message comes in
  + use imap's IDLE to receive push email notification
    + compared to polling:
      + faster
      + less resources

* quickstart

** install imapfilter

#+begin_src bash
  sudo apt install -y imapfilter
#+end_src

** imapfilter configuration

be sure to modify the ALL CAPS parts for your accounts

create =idler.lua= with the following content:

#+begin_src lua :tangle idler.lua
  -- run an external process, that outputs the password
  -- examples:
  -- local handle = io.popen('emacsclient -e \'(some-command-to-get-your-password-from-auth-source)\'')
  -- local handle = io.popen('gpg -q --for-your-eyes-only --no-tty -d /path/to/file.gpg')
  local handle = io.popen('pass email/USER@EXAMPLE.COM')
  local password = handle:read()
  handle:close()

  -- setup the corosponding IMAP-Account
  account = IMAP {
    server = 'EXAMPLE.COM',
    username = 'USER',
    password = password,
    ssl = 'auto'
  }

  while true do
    -- wait for new Mail. This halts the running program, until new
    -- mail arrives. change "Inbox" to any desired folder/mailbox
    success = account.Inbox:enter_idle()
    if success then
        -- update local maildir and mu4e's index (emacs-server needs
        -- to be running for this to work)
        os.execute('mbsync -a')
        os.execute('emacsclient -e "(mu4e-update-index)"')
    end
  end
#+end_src

put =idler.lua= in the right place:

#+begin_src bash
  mkdir -p ~/.config/imapfilter
  cp idler.lua ~/.config/imapfilter
#+end_src

** ensure imapfilter is running

*** run imapfilter as a service

create =imapfilter.service= with the following content:

#+begin_src systemd :tangle imapfilter.service
[Unit]
Description=IMAPFilter Service
After=network.target

[Service]
ExecStart=/usr/bin/imapfilter -c %h/.config/imapfilter/idler.lua
Restart=always
RestartSec=5

[Install]
WantedBy=default.target
#+end_src

copy =imapfilter.service= to the right place, and load the service:

#+begin_src bash
  mkdir -p ~/.config/systemd/user
  cp imapfilter.service ~/.config/systemd/user
  systemctl --user daemon-reload
  systemctl --user enable imapfilter.service
  systemctl --user start imapfilter.service
#+end_src

make sure the service started correctly:

#+begin_src bash
  systemctl --user status imapfilter.status
#+end_src

should give output that looks like the following:

#+begin_example
● imapfilter.service - IMAPFilter Service
     Loaded: loaded (/home/ozzloy/.config/systemd/user/imapfilter.service; enabled; preset: enabled)
     Active: active (running) since Sun 2024-06-02 22:13:38 PDT; 14min ago
   Main PID: 329788 (imapfilter)
      Tasks: 1 (limit: 76818)
     Memory: 1.7M (peak: 7.5M)
        CPU: 194ms
     CGroup: /user.slice/user-1000.slice/user@1000.service/app.slice/imapfilter.service
             └─329788 /usr/bin/imapfilter -c /home/ozzloy/.config/imapfi…
#+end_example

the important part is that it is =active (running)=.

* cleanup spac/emacs config

in emacs config, you no longer need ~mu4e-get-mail-command~ or
~mu4e-update-interval~.

setting ~mu4e-get-mail-command~ to ~"true"~ makes it a no-op, and is
its default value.
#+begin_src elisp
   mu4e-get-mail-command "true" ;; COMMENT OR SET TO "true"
  ;; mu4e-update-interval  300 ;; COMMENT THIS OUT, OR REMOVE IT
#+end_src

** make spacemacs open server
in =.spacemacs=, in =dotspacemacs/init=
#+begin_src elisp
  (defun dotspacemacs/init ()
    ;; ...
    (setq
     ;; ...
     dotspacemacs-enable-server t
     ;; ...
     )
     ;; ...
  )
#+end_src
=dotspacemacs-enable-server t=

* code taken from here initially

[[https://www.reddit.com/r/emacs/comments/mq2ff7/mu4e_imapserverpush_with_imapfilter/]]
